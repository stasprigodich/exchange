//
//  EXCurrencyManager.h
//  exchange
//
//  Created by Stanislav Prigodich on 12/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    EXCurrencyTypeEUR,
    EXCurrencyTypeGBP,
    EXCurrencyTypeUSD
}  EXCurrencyType;

@interface EXCurrencyManager : NSObject

+ (instancetype)sharedInstance;

- (void)updateRates;

- (double)getRateForCurrency:(EXCurrencyType)currencyType toCurrency:(EXCurrencyType)toCurrencyType;

- (NSString*)getRateStringForCurrency:(EXCurrencyType)currencyType toCurrency:(EXCurrencyType)toCurrencyType;

- (NSString*)getCurrencySymbol:(EXCurrencyType)currencyType;

- (double)getAccountValueForCurrency:(EXCurrencyType)currencyType;

- (void)changeAccountValue:(double)value currencyType:(EXCurrencyType)currencyType;

@end
