//
//  EXCurrencyManager.m
//  exchange
//
//  Created by Stanislav Prigodich on 12/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import "EXCurrencyManager.h"
#import <CoreData/CoreData.h>


@interface EXCurrencyManager() <NSXMLParserDelegate>

@end

@implementation EXCurrencyManager

+ (instancetype)sharedInstance {
    static EXCurrencyManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[EXCurrencyManager alloc] init];
    });
    return sharedInstance;
}

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - public methods

- (void)updateRates {
    NSURL *yoururl = [NSURL URLWithString:@"http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml"];
    NSXMLParser *parser = [[NSXMLParser alloc] initWithContentsOfURL:yoururl];
    parser.delegate = self;
    [parser parse];
}

- (double)getRateForCurrency:(EXCurrencyType)currencyType toCurrency:(EXCurrencyType)toCurrencyType {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EuroRate"];
    NSArray* rates = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSDictionary* euroRate = rates.firstObject;
    
    if (euroRate) {
        double eurUsd = [[euroRate valueForKey:@"usd"] doubleValue];
        double eurGbp = [[euroRate valueForKey:@"gbp"] doubleValue];
        if (currencyType == EXCurrencyTypeEUR && toCurrencyType == EXCurrencyTypeGBP) {
            return eurGbp;
        } else if (currencyType == EXCurrencyTypeEUR && toCurrencyType == EXCurrencyTypeUSD) {
            return eurUsd;
        } else if (currencyType == EXCurrencyTypeGBP && toCurrencyType == EXCurrencyTypeEUR) {
            return 1/eurGbp;
        } else if (currencyType == EXCurrencyTypeGBP && toCurrencyType == EXCurrencyTypeUSD) {
            return eurUsd / eurGbp;
        } else if (currencyType == EXCurrencyTypeUSD && toCurrencyType == EXCurrencyTypeEUR) {
            return 1/eurUsd;
        } else if (currencyType == EXCurrencyTypeUSD && toCurrencyType == EXCurrencyTypeGBP) {
            return eurGbp / eurUsd;
        }
    }
    return 1.0;
}

- (NSString*)getRateStringForCurrency:(EXCurrencyType)currencyType toCurrency:(EXCurrencyType)toCurrencyType {
    double rate = [self getRateForCurrency:currencyType toCurrency:toCurrencyType];
    NSString* rateString = [NSString stringWithFormat:@"%@1 = %@%.02f", [self getCurrencySymbol:currencyType], [self getCurrencySymbol:toCurrencyType], rate];
    return rateString;
}

- (NSString*)getCurrencySymbol:(EXCurrencyType)currencyType {
    switch (currencyType) {
        case EXCurrencyTypeUSD:
            return @"$";
        case EXCurrencyTypeGBP:
            return @"£";
        case EXCurrencyTypeEUR:
            return @"€";
    }
}

- (double)getAccountValueForCurrency:(EXCurrencyType)currencyType {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Account"];
    NSArray* accounts = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSDictionary* account = accounts.firstObject;
    
    if (account) {
        switch (currencyType) {
            case EXCurrencyTypeUSD:
                return [[account valueForKey:@"usd"] doubleValue];
            case EXCurrencyTypeGBP:
                return [[account valueForKey:@"gbp"] doubleValue];
            case EXCurrencyTypeEUR:
                return [[account valueForKey:@"eur"] doubleValue];
        }
    }
    return 100.0;
}

- (void)changeAccountValue:(double)value currencyType:(EXCurrencyType)currencyType {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Account"];
    NSArray* accounts = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSDictionary* account = accounts.firstObject;
    double currentValue = [self getAccountValueForCurrency:currencyType];
    if (account) {
        switch (currencyType) {
            case EXCurrencyTypeUSD:
                [account setValue:@(currentValue + value) forKey:@"usd"];
                break;
            case EXCurrencyTypeGBP:
                [account setValue:@(currentValue + value) forKey:@"gbp"];
                break;
            case EXCurrencyTypeEUR:
                [account setValue:@(currentValue + value) forKey:@"eur"];
                break;
        }
    } else {
        NSManagedObject *newAccount = [NSEntityDescription insertNewObjectForEntityForName:@"Account" inManagedObjectContext:context];
        switch (currencyType) {
            case EXCurrencyTypeUSD:
                [newAccount setValue:@(currentValue + value) forKey:@"usd"];
                break;
            case EXCurrencyTypeGBP:
                [newAccount setValue:@(currentValue + value) forKey:@"gbp"];
                break;
            case EXCurrencyTypeEUR:
                [newAccount setValue:@(currentValue + value) forKey:@"eur"];
                break;
        }
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

#pragma mark - NSXMLParserDelegate

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    if ([elementName isEqualToString:@"Cube"]) {
        if ([attributeDict[@"currency"] isEqualToString:@"USD"]) {
            NSString* usdRate = attributeDict[@"rate"];
            [self saveRate:[usdRate doubleValue] currencyType:EXCurrencyTypeUSD];
        } else if ([attributeDict[@"currency"] isEqualToString:@"GBP"]) {
            NSString* gbpRate = attributeDict[@"rate"];
            [self saveRate:[gbpRate doubleValue] currencyType:EXCurrencyTypeGBP];
        }
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RatesDidUpdate" object:nil];
    });
}

#pragma mark - private methods

- (void)saveRate:(double)value currencyType:(EXCurrencyType)currencyType {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"EuroRate"];
    NSArray* rates = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSDictionary* euroRate = rates.firstObject;
    
    if (euroRate) {
        switch (currencyType) {
            case EXCurrencyTypeUSD:
                [euroRate setValue:@(value) forKey:@"usd"];
                break;
            case EXCurrencyTypeGBP:
                [euroRate setValue:@(value) forKey:@"gbp"];
                break;
            default:
                break;
        }
    } else {
        NSManagedObject *newEuroRate = [NSEntityDescription insertNewObjectForEntityForName:@"EuroRate" inManagedObjectContext:context];
        switch (currencyType) {
            case EXCurrencyTypeUSD:
                [newEuroRate setValue:@(value) forKey:@"usd"];
                break;
            case EXCurrencyTypeGBP:
                [newEuroRate setValue:@(value) forKey:@"gbp"];
                break;
            default:
                break;
        }
    }
    
    NSError *error = nil;
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
}

@end
