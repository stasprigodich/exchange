//
//  main.m
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EXAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EXAppDelegate class]));
    }
}
