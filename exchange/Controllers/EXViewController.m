//
//  EXViewController.m
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import "EXViewController.h"
#import "EXCurrencyListView.h"
#import "EXCurrencyManager.h"

@interface EXViewController () <EXCurrencyListViewDelegate>

@property (weak, nonatomic) IBOutlet EXCurrencyListView *sellingCurrencyListView;
@property (weak, nonatomic) IBOutlet EXCurrencyListView *buyingCurrencyListView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *exchangeButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cancelButton;

@property (assign, nonatomic) double currentSellingValue;
@property (assign, nonatomic) double currentBuyingValue;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@property (weak, nonatomic) IBOutlet UILabel *rateLabel;

@property (nonatomic, assign) BOOL isLoaded;

@end

@implementation EXViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self commonInit];
    [self observeNotifications];
    [self updateRates];
    [self autoRefresh];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (!self.isLoaded) {
        self.sellingCurrencyListView.isSellingCurrency = YES;
        self.buyingCurrencyListView.isSellingCurrency = NO;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Notifications

- (void)keyboardWasShownNotification:(NSNotification *)notification {
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    int height = MIN(keyboardSize.height,keyboardSize.width);
    self.bottomConstraint.constant = height;
    self.sellingCurrencyListView.hidden = NO;
    self.buyingCurrencyListView.hidden = NO;
}

- (void)ratesDidUpdateNotification {
    if (!self.isLoaded) {
        [self.sellingCurrencyListView makeActive];
    }
    EXCurrencyType sellingType = self.sellingCurrencyListView.selectedCurrencyType;
    EXCurrencyType buyingType = self.buyingCurrencyListView.selectedCurrencyType;
    self.rateLabel.text = [[EXCurrencyManager sharedInstance] getRateStringForCurrency:sellingType toCurrency:buyingType];
    [self.buyingCurrencyListView updateRates:sellingType];
    
    if (self.sellingCurrencyListView.isActive) {
        [self.buyingCurrencyListView clearAllValues];
        [self.buyingCurrencyListView setNewValues:self.sellingCurrencyListView.currentValue oppositeCurrency:self.sellingCurrencyListView.selectedCurrencyType];
    } else {
        [self.sellingCurrencyListView clearAllValues];
        [self.sellingCurrencyListView setNewValues:self.buyingCurrencyListView.currentValue oppositeCurrency:self.buyingCurrencyListView.selectedCurrencyType];
    }
    [self updateExchangeButtonEnabled];
    
    self.isLoaded = YES;
}

- (void)changeActiveViewNotification:(NSNotification*)notification {
    NSDictionary* userInfo = notification.userInfo;
    BOOL isSellingView = [userInfo[@"isSellingView"] boolValue];
    [self makeActiveView:isSellingView];
}

#pragma mark - gestures

- (void)handleSellingViewTap:(UITapGestureRecognizer *)recognizer {
    [self makeActiveView:YES];
}

- (void)handleBuyingViewTap:(UITapGestureRecognizer *)recognizer {
    [self makeActiveView:NO];
}

#pragma mark - actions

- (IBAction)exchangeButtonTapped:(id)sender {
    double sellingValue = self.sellingCurrencyListView.currentValue;
    double buyingValue = self.buyingCurrencyListView.currentValue;
    
    EXCurrencyType sellingType = self.sellingCurrencyListView.selectedCurrencyType;
    EXCurrencyType buyingType = self.buyingCurrencyListView.selectedCurrencyType;
    
    [[EXCurrencyManager sharedInstance] changeAccountValue:-sellingValue currencyType:sellingType];
    [[EXCurrencyManager sharedInstance] changeAccountValue:buyingValue currencyType:buyingType];
    
    [self.sellingCurrencyListView updateAccounts];
    [self.buyingCurrencyListView updateAccounts];
    [self.sellingCurrencyListView clearAllValues];
    [self.buyingCurrencyListView clearAllValues];
    [self updateExchangeButtonEnabled];
}

- (IBAction)cancelButtonTapped:(id)sender {
    [self.sellingCurrencyListView clearAllValues];
    [self.buyingCurrencyListView clearAllValues];
    [self updateExchangeButtonEnabled];
}

#pragma mark - EXCurrencyListViewDelegate

- (void)valueChanged:(double)value isSellingCurrency:(BOOL)isSellingCurrency {
    if (isSellingCurrency) {
        [self.buyingCurrencyListView setNewValues:self.sellingCurrencyListView.currentValue oppositeCurrency:self.sellingCurrencyListView.selectedCurrencyType];
        [self updateExchangeButtonEnabled];
    } else {
        [self.sellingCurrencyListView setNewValues:self.buyingCurrencyListView.currentValue oppositeCurrency:self.buyingCurrencyListView.selectedCurrencyType];
        [self updateExchangeButtonEnabled];
    }
}

- (void)currencyChanged:(BOOL)isSellingCurrency {
    EXCurrencyType sellingType = self.sellingCurrencyListView.selectedCurrencyType;
    EXCurrencyType buyingType = self.buyingCurrencyListView.selectedCurrencyType;
    
    [self updateExchangeButtonEnabled];
    
    self.rateLabel.text = [[EXCurrencyManager sharedInstance] getRateStringForCurrency:sellingType toCurrency:buyingType];
    [self.buyingCurrencyListView updateRates:sellingType];
    
    if ((isSellingCurrency && self.sellingCurrencyListView.isActive) || (!isSellingCurrency && self.buyingCurrencyListView.isActive)) {
        [self.sellingCurrencyListView clearAllValues];
        [self.buyingCurrencyListView clearAllValues];
        [self updateExchangeButtonEnabled];
    } else {
        if (isSellingCurrency) {
            [self.sellingCurrencyListView clearAllValues];
            [self.sellingCurrencyListView setNewValues:self.buyingCurrencyListView.currentValue oppositeCurrency:buyingType];
        } else {
            [self.buyingCurrencyListView clearAllValues];
            [self.buyingCurrencyListView setNewValues:self.sellingCurrencyListView.currentValue oppositeCurrency:sellingType];
        }
    }
}

#pragma mark - private methods

- (void)commonInit {
    self.rateLabel.text = @"-";
    self.exchangeButton.enabled = NO;
    
    self.sellingCurrencyListView.isActive = YES;
    self.buyingCurrencyListView.isActive = NO;
    
    self.sellingCurrencyListView.hidden = YES;
    self.buyingCurrencyListView.hidden = YES;
    
    self.sellingCurrencyListView.delegate = self;
    self.buyingCurrencyListView.delegate = self;
    
    UITapGestureRecognizer *sellingViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSellingViewTap:)];
    [self.sellingCurrencyListView addGestureRecognizer:sellingViewTap];
    
    UITapGestureRecognizer *buyingViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBuyingViewTap:)];
    [self.buyingCurrencyListView addGestureRecognizer:buyingViewTap];
}

- (void)updateRates {
    [[EXCurrencyManager sharedInstance] updateRates];
}

- (void)makeActiveView:(BOOL)isSellingView {
    self.sellingCurrencyListView.isActive = isSellingView;
    self.buyingCurrencyListView.isActive = !isSellingView;
    if (isSellingView) {
        [self.sellingCurrencyListView makeActive];
    } else {
        [self.buyingCurrencyListView makeActive];
    }
}

- (void)observeNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShownNotification:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(ratesDidUpdateNotification)
                                                 name:@"RatesDidUpdate"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(changeActiveViewNotification:)
                                                 name:@"MakeActiveView"
                                               object:nil];
}

- (void)autoRefresh {
    [NSTimer scheduledTimerWithTimeInterval:30.0
                                     target:self
                                   selector:@selector(updateRates)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)updateExchangeButtonEnabled {
    double currentValue = [[EXCurrencyManager sharedInstance] getAccountValueForCurrency:self.sellingCurrencyListView.selectedCurrencyType];
    self.exchangeButton.enabled = self.sellingCurrencyListView.selectedCurrencyType != self.buyingCurrencyListView.selectedCurrencyType && self.sellingCurrencyListView.currentValue <= currentValue && self.sellingCurrencyListView.currentValue > 0;
}

@end
