//
//  NibView.m
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import "NibView.h"

@implementation NibView

- (instancetype)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        [self setup];
    }
    return self;
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self setup];
    }
    return self;
}

- (void)setup {
    self.translatesAutoresizingMaskIntoConstraints = NO;
    UIView* contentView = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil] firstObject];
    contentView.backgroundColor = [UIColor clearColor];
    contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:contentView];
    for (NSString* constraintFormat in @[@"V:|[contentView]|", @"H:|[contentView]|"]) {
        NSArray* constraints = [NSLayoutConstraint constraintsWithVisualFormat:constraintFormat options:0 metrics:nil views:@{@"contentView": contentView}];
        [self addConstraints:constraints];
    }
}

@end
