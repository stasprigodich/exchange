//
//  EXCurrencyListView.m
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import "EXCurrencyListView.h"

@interface EXCurrencyListView() <UIScrollViewDelegate, EXCurrencyViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) NSArray<EXCurrencyView*>* currencyViews;

@property (nonatomic, assign) NSInteger currentIndex;
@property (nonatomic, assign) NSInteger pageNumber;

@property (nonatomic, strong) EXCurrencyView* selectedCurrencyView;

@end

@implementation EXCurrencyListView

@synthesize isSellingCurrency = _isSellingCurrency;

- (void)setup {
    [super setup];
    
    [self initCurrencyList];
    self.pageControl.numberOfPages = self.currencyViews.count - 2;
    self.scrollView.delegate = self;
}

- (void)initCurrencyList {
    EXCurrencyView* fakeCurrencyView1 = [[EXCurrencyView alloc] initWithCurrencyType:EXCurrencyTypeEUR delegate:self];
    EXCurrencyView* currencyView1 = [[EXCurrencyView alloc] initWithCurrencyType:EXCurrencyTypeEUR delegate:self];
    EXCurrencyView* currencyView2 = [[EXCurrencyView alloc] initWithCurrencyType:EXCurrencyTypeGBP delegate:self];
    EXCurrencyView* currencyView3 = [[EXCurrencyView alloc] initWithCurrencyType:EXCurrencyTypeUSD delegate:self];
    EXCurrencyView* fakeCurrencyView3 = [[EXCurrencyView alloc] initWithCurrencyType:EXCurrencyTypeUSD delegate:self];
    
    self.currencyViews = @[fakeCurrencyView3 ,currencyView1, currencyView2, currencyView3, fakeCurrencyView1];
    
    for (int i = 0; i < self.currencyViews.count; i++) {
        EXCurrencyView* currencyView = self.currencyViews[i];
        [self.scrollView addSubview:currencyView];
        [currencyView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[currencyView(width)]" options:0 metrics:@{@"width": @([UIScreen mainScreen].bounds.size.width)} views:@{@"currencyView": currencyView}]];
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[currencyView]|" options:0 metrics:nil views:@{@"currencyView": currencyView}]];
        [self.scrollView addConstraint:[NSLayoutConstraint constraintWithItem:currencyView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.scrollView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0]];
    }
    for (int i = 0; i < self.currencyViews.count - 1; i++) {
        EXCurrencyView* currencyView1 = self.currencyViews[i];
        EXCurrencyView* currencyView2 = self.currencyViews[i+1];
        [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[currencyView1]-(0)-[currencyView2]" options:0 metrics:nil views:@{@"currencyView1": currencyView1, @"currencyView2": currencyView2}]];
    }
    EXCurrencyView* currencyViewFirst = self.currencyViews.firstObject;
    [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[currencyViewFirst]" options:0 metrics:nil views:@{@"currencyViewFirst": currencyViewFirst}]];
    
    EXCurrencyView* currencyViewLast = self.currencyViews.lastObject;
    [self.scrollView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[currencyViewLast]|" options:0 metrics:nil views:@{@"currencyViewLast": currencyViewLast}]];
}

#pragma mark - getters and setters

- (BOOL)isSellingCurrency {
    return _isSellingCurrency;
}

- (void)setIsSellingCurrency:(BOOL)isSellingCurrency {
    for (EXCurrencyView* currencyView in self.currencyViews) {
        currencyView.isSellingCurrency = isSellingCurrency;
    }
    if (!isSellingCurrency) {
        self.selectedCurrencyType = ((EXCurrencyView*)self.currencyViews[2]).currencyType;
        self.pageControl.currentPage = 1;
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width*2, 0);
    } else {
        self.selectedCurrencyType = ((EXCurrencyView*)self.currencyViews[1]).currencyType;
        self.pageControl.currentPage = 0;
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
        [self makeActive];
    }
    _isSellingCurrency = isSellingCurrency;
}

- (NSInteger)currentIndex {
    return self.scrollView.contentOffset.x  / self.scrollView.frame.size.width;
}

- (NSInteger)pageNumber {
    return (self.scrollView.contentOffset.x - self.scrollView.frame.size.width) / self.scrollView.frame.size.width;
}

- (EXCurrencyView*)selectedCurrencyView {
    return self.currencyViews[self.currentIndex];
}

#pragma mark - public methods

- (void)setNewValues:(double)oppositeValue oppositeCurrency:(EXCurrencyType)oppositeCurrency {
    for (int i = 0; i < self.currencyViews.count; i++) {
        EXCurrencyView* currencyView = self.currencyViews[i];
        double value = oppositeValue * [[EXCurrencyManager sharedInstance] getRateForCurrency:oppositeCurrency toCurrency:currencyView.currencyType];
        [currencyView setCurrentValue:value];
        if (i == self.currentIndex) {
            self.currentValue = value;
        }
    }
    for (EXCurrencyView* currencyView in self.currencyViews) {
        double value = oppositeValue * [[EXCurrencyManager sharedInstance] getRateForCurrency:oppositeCurrency toCurrency:currencyView.currencyType];
        [currencyView setCurrentValue:value];
    }
}

- (void)makeActive {
    [self.selectedCurrencyView makeActive];
}

- (void)clearAllValues {
    self.currentValue = 0;
    for (EXCurrencyView* currencyView in self.currencyViews) {
        [currencyView clearValue];
    }
}

- (void)updateAccounts {
    for (EXCurrencyView* currencyView in self.currencyViews) {
        [currencyView updateAccount];
    }
}

- (void)updateRates:(EXCurrencyType)oppositeCurrency {
    for (EXCurrencyView* currencyView in self.currencyViews) {
        [currencyView updateRate:oppositeCurrency];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self scrollDidStop];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self scrollDidStop];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.scrollView.contentOffset.x <= 0) {
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width * (self.currencyViews.count - 2), 0);
    }
    if (self.scrollView.contentOffset.x >= self.scrollView.frame.size.width * (self.currencyViews.count - 1)) {
        self.scrollView.contentOffset = CGPointMake(self.scrollView.frame.size.width, 0);
    }

}

- (void)scrollDidStop {
    EXCurrencyType type = self.selectedCurrencyView.currencyType;
    if (type != self.selectedCurrencyType) {
        self.currentValue = self.selectedCurrencyView.currentValue;
        self.selectedCurrencyType = type;
        if (self.isActive) {
            [self.selectedCurrencyView makeActive];
        }
        [self.delegate currencyChanged:self.isSellingCurrency];
    }
    
    self.pageControl.currentPage = self.pageNumber;
}

#pragma mark - EXCurrencyViewDelegate

- (void)valueChanged:(double)value currencyType:(EXCurrencyType)currencyType {
    self.currentValue = value;
    [self.delegate valueChanged:value isSellingCurrency:self.isSellingCurrency];
}


@end
