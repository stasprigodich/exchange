//
//  EXCurrencyListView.h
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NibView.h"
#import "EXCurrencyView.h"

@protocol EXCurrencyListViewDelegate <NSObject>

@required
- (void)valueChanged:(double)value isSellingCurrency:(BOOL)isSellingCurrency;
- (void)currencyChanged:(BOOL)isSellingCurrency;

@end


@interface EXCurrencyListView : NibView

@property (nonatomic, assign) EXCurrencyType selectedCurrencyType;
@property (nonatomic, assign) double currentValue;
@property (nonatomic, assign) BOOL isSellingCurrency;
@property (nonatomic, assign) BOOL isActive;

- (void)setNewValues:(double)oppositeValue oppositeCurrency:(EXCurrencyType)oppositeCurrency;
- (void)makeActive;
- (void)clearAllValues;
- (void)updateAccounts;
- (void)updateRates:(EXCurrencyType)oppositeCurrency;

@property (weak, nonatomic) id<EXCurrencyListViewDelegate> delegate;

@end
