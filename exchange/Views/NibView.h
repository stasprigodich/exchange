//
//  NibView.h
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NibView : UIView

- (void)setup;

@end
