//
//  EXCurrencyView.h
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NibView.h"
#import "EXCurrencyManager.h"

@protocol EXCurrencyViewDelegate <NSObject>

@required
- (void)valueChanged:(double)value currencyType:(EXCurrencyType)currencyType;

@end

@interface EXCurrencyView : NibView

- (instancetype)initWithCurrencyType:(EXCurrencyType)currencyType delegate:(id<EXCurrencyViewDelegate>)delegate;

@property (nonatomic, assign) double currentValue;

- (void)clearValue;
- (void)makeActive;
- (void)updateAccount;
- (void)updateRate:(EXCurrencyType)oppositeCurrency;

@property (nonatomic, assign) BOOL isSellingCurrency;
@property (nonatomic, assign) EXCurrencyType currencyType;

@property (weak, nonatomic) id<EXCurrencyViewDelegate> delegate;

@end
