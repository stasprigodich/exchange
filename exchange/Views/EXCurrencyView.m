//
//  EXCurrencyView.m
//  exchange
//
//  Created by Stanislav Prigodich on 11/02/2017.
//  Copyright © 2017 Prigodich. All rights reserved.
//

#import "EXCurrencyView.h"

@interface EXCurrencyView() <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *currencyTypeLabel;
@property (weak, nonatomic) IBOutlet UITextField *valueTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *valueTextFieldWidthConstraint;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;

@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UILabel *signLabel;

@end

@implementation EXCurrencyView

@synthesize isSellingCurrency = _isSellingCurrency;

- (void)setup {
    [super setup];
    self.valueTextField.delegate = self;
    [self.valueTextField addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    self.signLabel.text = @"";
}

- (instancetype)initWithCurrencyType:(EXCurrencyType)currencyType delegate:(id<EXCurrencyViewDelegate>)delegate {
    if ((self = [super initWithFrame:CGRectZero])) {
        self.delegate = delegate;
        self.currencyType = currencyType;
        switch (currencyType) {
            case EXCurrencyTypeEUR:
                self.currencyTypeLabel.text = @"EUR";
                break;
            case EXCurrencyTypeGBP:
                self.currencyTypeLabel.text = @"GBP";
                break;
            case EXCurrencyTypeUSD:
                self.currencyTypeLabel.text = @"USD";
                break;
        }
        double amount = [[EXCurrencyManager sharedInstance] getAccountValueForCurrency:currencyType];
        self.accountLabel.text = [NSString stringWithFormat:@"You have %@%.02f", [[EXCurrencyManager sharedInstance] getCurrencySymbol:currencyType], amount];
    }
    return self;
}

#pragma mark - getters and setters

- (BOOL)isSellingCurrency {
    return _isSellingCurrency;
}

- (void)setIsSellingCurrency:(BOOL)isSellingCurrency {
    if (isSellingCurrency) {
        self.rateLabel.hidden = YES;
    }
    _isSellingCurrency = isSellingCurrency;
}

- (double)currentValue {
    return [[self.valueTextField.text stringByReplacingOccurrencesOfString:@"," withString:@"."] doubleValue];
}

- (void)setCurrentValue:(double)currentValue {
    if (currentValue > 0) {
        NSString* stringValue = [NSString stringWithFormat:@"%.02f", currentValue];
        if ([[self getDecimalSeparator] isEqualToString:@","]) {
            self.valueTextField.text = [stringValue stringByReplacingOccurrencesOfString:@"." withString:@","];
        } else {
            self.valueTextField.text = [stringValue stringByReplacingOccurrencesOfString:@"," withString:@"."];
        }
        [self updateTextField];
    } else {
        [self clearValue];
    }
}

#pragma mark - public methods

- (void)clearValue {
    self.valueTextField.text = @"";
    [self updateTextField];
}

- (void)makeActive {
    [self.valueTextField becomeFirstResponder];
}

- (void)updateAccount {
    self.accountLabel.text = [NSString stringWithFormat:@"You have %@%.02f", [[EXCurrencyManager sharedInstance] getCurrencySymbol:self.currencyType], [[EXCurrencyManager sharedInstance] getAccountValueForCurrency:self.currencyType]];
}

- (void)updateRate:(EXCurrencyType)oppositeCurrency {
    self.rateLabel.text = [[EXCurrencyManager sharedInstance] getRateStringForCurrency:self.currencyType toCurrency:oppositeCurrency];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidChange:(UITextField*)textField {
    [self updateTextField];
    [self.delegate valueChanged:self.currentValue currencyType:self.currencyType];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MakeActiveView" object:nil userInfo:@{@"isSellingView": @(self.isSellingCurrency)}];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return textField.text.length < 10 || string.length == 0;
}

#pragma mark - private methods

- (void)updateTextField {
    self.valueTextFieldWidthConstraint.constant = MAX(15.0, [self widthOfString:self.valueTextField.text withFont:self.valueTextField.font]) + 8;
    if (self.valueTextField.text.length > 0) {
        self.signLabel.text = self.isSellingCurrency ? @"-" : @"+";
    } else {
        self.signLabel.text = @"";
    }
    if (self.isSellingCurrency) {
        double currentValue = [[EXCurrencyManager sharedInstance] getAccountValueForCurrency:self.currencyType];
        self.accountLabel.textColor = self.currentValue > currentValue ? [UIColor redColor] : [UIColor lightGrayColor];
    }
}

- (CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

- (NSString *)getDecimalSeparator {
    return [[NSLocale currentLocale] objectForKey:NSLocaleDecimalSeparator];
}


@end
